<?php 
header("content-type: application/json");
header("Access-Control-Allow-Origin: *");
function base64ToBinary($data) {
    return base64_decode($data);
}

function decryptData($cipher, $iv, $key) {
    $tagLength = 16; // Length of the authentication tag in bytes
    $tag = substr($cipher, -$tagLength);
    $cipher = substr($cipher, 0, -$tagLength);

    $decryptedData = openssl_decrypt(
        $cipher,
        'aes-256-gcm',
        $key,
        OPENSSL_RAW_DATA,
        $iv,
        $tag
    );

    if ($decryptedData === false) {
        throw new Exception('Decryption failed: ' . openssl_error_string());
    }

    return $decryptedData;
}

// $data = json_decode(file_get_contents('php://input'), true);
$pdata = file_get_contents('php://input');
$data = json_decode(base64ToBinary($pdata),true);
$cipher = base64ToBinary($data['cipher']);
$iv = base64ToBinary($data['iv']);
$key = base64ToBinary($data['key']);

try {
    $decryptedData = decryptData($cipher, $iv, $key);
    echo json_encode([
        "Decrypted_data"=>$decryptedData
    ]);
} catch (Exception $e) {
    echo json_encode([
        "Error"=>$e->getMessage()
    ]);
}

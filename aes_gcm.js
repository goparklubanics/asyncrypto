async function generateKey() {
    const key = await crypto.subtle.generateKey(
        {
            name: "AES-GCM",
            length: 256,
        },
        true, // extractable for serialization
        ["encrypt", "decrypt"]
    );
    return key;
}

async function encryptData(key, data) {
    const iv = crypto.getRandomValues(new Uint8Array(12));
    const encodedData = new TextEncoder().encode(data);
    
    const cipherBuffer = await crypto.subtle.encrypt(
        {
            name: "AES-GCM",
            iv: iv
        },
        key,
        encodedData
    );

    return {
        cipher: new Uint8Array(cipherBuffer),
        iv: iv
    };
}

async function exportKey(key) {
    const exportedKey = await crypto.subtle.exportKey('raw', key);
    return new Uint8Array(exportedKey);
}

function arrayBufferToBase64(buffer) {
    let binary = '';
    const bytes = new Uint8Array(buffer);
    bytes.forEach((b) => binary += String.fromCharCode(b));
    return btoa(binary);
}

async function main() {
    const key = await generateKey();
    const data = "This is a secret message!";
    const encryptedData = await encryptData(key, data);
    const exportedKey = await exportKey(key);
    
    const payload = {
        cipher: arrayBufferToBase64(encryptedData.cipher),
        iv: arrayBufferToBase64(encryptedData.iv),
        key: arrayBufferToBase64(exportedKey)
    };
    console.log("secret payload:",btoa(JSON.stringify(payload)));
    // Send the payload to the PHP backend
    await fetch('http://localhost/aes_gcm/decryptor.php', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: btoa(JSON.stringify(payload))
    }).then((response) => {
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        return response.text();
    }).then((data) => {
        console.log(data);
    }).catch((error) => {
        console.error(error);
    });
}

main();